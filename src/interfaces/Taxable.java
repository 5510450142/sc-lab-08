package interfaces;

public interface Taxable {

	public double getTaxable();
}
