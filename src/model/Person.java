package model;

import interfaces.Measureable;
import interfaces.Taxable;

public class Person implements Measureable, Taxable {

	private String name;
	private double balance;
	private double height;

	public Person(String name, double height, double balance) {
		this.name = name;
		this.balance = balance;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double getTaxable() {
		double tax = 0;
		if (balance <= 300000){
			tax = (5*balance)/100;
		}
		else {
			double amount = balance - 300000;
			double taxfirst = (300000*5)/100;
			double taxsecond = (amount*10)/100;
			tax = taxfirst + taxsecond;
		}
		return tax;
	}

	@Override
	public double getMeasure() {
		return height;
	}
}
